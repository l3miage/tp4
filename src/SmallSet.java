import java.util.Arrays;

public class SmallSet {

    private boolean[] tab = new boolean[256];

    public SmallSet() {
        for (int i = 0; i <= 255; i++) {
            tab[i] = false;
        }
    }

    public SmallSet(boolean[] t) {
        for (int i = 0; i <= 255; i++) {
            tab[i] = t[i];
        }
    }

    public int size() {
        int count = 0;
        for (int i = 0; i <= 255; i++) {
            if (tab[i]) count++;
        }
        return count;
    }

    public boolean contains (int x) {
        if (x >= 0 && x <= 255) return tab[x];
        else return false;
    }

    public boolean isEmpty() {
        for (int i = 0; i <= 255; i++) {
            if (tab[i]) return false;
        }
        return true;
    }

    public void add(int x) {
        if (x >= 0 && x <= 255) tab[x] = true;
    }

    public void remove(int x) {
        if (x >= 0 && x <= 255) tab[x] = false;
    }

    public void addInterval(int deb, int fin) {
        if (deb >= 0 && fin <= 255 && fin > deb) {
            for (int i = deb; i <= fin; i++) {
                tab[i] = true;
            }
        }
    }

    public void removeInterval(int deb, int fin) {
        if (deb >= 0 && fin <= 255 && fin > deb) {
            for (int i = deb; i <= fin; i++) {
                tab[i] = false;
            }
        }
    }

    public void union(SmallSet set2) {
        for (int i = 0; i <= 255; i++) {
            if (set2.contains(i)) tab[i] = true;
        }
    }

    public void intersection(SmallSet set2) {
        for (int i = 0; i <= 255; i++) {
            tab[i] = set2.contains(i) && tab[i];
        }
    }

    public void difference(SmallSet set2) {
        for (int i = 0; i <= 255; i++) {
            if (set2.contains(i) && tab[i]) tab[i] = false;
        }
    }

    public void symmetricDifference(SmallSet set2) {
        for (int i = 0; i <= 255; i++) {
            tab[i] = !set2.contains(i) || !tab[i];
        }
    }

    public void complement() {
        for (int i = 0; i <= 255; i++) {
            tab[i] = !tab[i];
        }
    }

    public void clear() {
        for (int i = 0; i <= 255; i++) {
            tab[i] = false;
        }
    }

    public boolean isIncludedIn(SmallSet set2) {
        for (int i = 0; i <= 255; i++) {
            if (tab[i] && !set2.contains(i)) return false;
        }
        return true;
    }

    public SmallSet clone() {
        return new SmallSet(tab);
    }

    @Override
    public boolean equals(Object set2) {
        if (this == set2) return true;
        if (set2 == null || getClass() != set2.getClass()) return false;
        SmallSet smallSet = (SmallSet) set2;
        return Arrays.equals(tab, smallSet.tab);
    }

    @Override
    public String toString() {
        String s = "éléments présents : ";
        for (int i = 0; i <= 255; i++) {
            if (tab[i]) {
                s = s + i + " ";
            }
        }
        return s;
    }


}
